#!/bin/bash

docker rm -f registry nm nx
docker rmi localhost:5000/nm localhost:5000/nx

docker build -t localhost:5000/nm ./docker -f ./docker/nm.dockerfile
docker build -t localhost:5000/nx ./docker -f ./docker/nx.dockerfile

docker-compose up -d registry
sleep 3s
docker push localhost:5000/nm
docker push localhost:5000/nx
docker-compose up -d nm nx
