#!/bin/bash
# root / kocev321
# This backup/restore script is necessary for Windows
# because /var/opt directory can not be mounted properly

glmain=spalna
contBackupDir=/var/opt/gitlab/backups
hostBackupDir=d:/gitlab/opt/gitlab/backups

backup() {
	# using SKIP=builds,artifacts,lfs,registry on gitlab:backup:create will save only 0.1mb
	docker exec -t $glmain gitlab-rake gitlab:backup:create
	docker exec -t $glmain bash -c "rm -f $contBackupDir/main_gitlab_backup.tar"
	lastFile=`docker exec $glmain bash -c "ls -t $contBackupDir/*.tar"`
	mkdir -p $hostBackupDir
	docker cp $glmain:$lastFile $hostBackupDir/main_gitlab_backup.tar
}

case ${1:0:1} in
	u|U )
		docker-compose up -d $glmain
		echo Starting gitlab...
		until [[ "`docker inspect -f {{.State.Health.Status}} $glmain`" == "healthy" ]]; do
			sleep 1m;
		done;
		docker cp d:/gitlab/etc/gitlab $glmain:/etc
		docker exec $glmain bash -c "chmod -R 600 /etc/gitlab"
		docker exec $glmain bash -c "chmod 644 /etc/gitlab/*.pub /etc/gitlab/ssl/*.crt"
		docker exec $glmain bash -c "chmod 755 /etc/gitlab/ssl /etc/gitlab/trusted-certs"
		docker exec $glmain gitlab-ctl restart sshd
		docker cp $hostBackupDir/main_gitlab_backup.tar $glmain:$contBackupDir/main_gitlab_backup.tar
		docker exec -t $glmain gitlab-rake gitlab:backup:restore force=yes
		docker cp $glmain:/etc/gitlab/ssl/$glmain.crt d:/gitlab/runner/config/certs/ca.crt
		docker-compose up -d runner
		if [[ "$(docker images -q localhost:5000/nm 2> /dev/null)" == "" ]]; then
			./republish-prods.sh
		else
			docker-compose up -d registry nm nx
		fi;
	;;
	d|D )
		# first run backup separately, only when needed
		docker-compose down
		runners=`docker ps -qa --filter=name=runner`
		[[ $runners ]] && docker rm -f $runners
	;;
	n|N )
		echo Upgrading gitlab-ce with new version...
		if [[ ! `docker ps -qa --filter=name=$glmain` ]]; then
			echo \'$glmain\' container is down. Run \'./run.sh up\' first
			exit
		fi;
		if [[ `docker inspect -f '{{.Config.Image}}' $glmain` =~ :([0-9]+\.[0-9]+\..+)$ ]]; then
			echo Current version: ${BASH_REMATCH[1]} # this is slow: docker exec spalna gitlab-rake gitlab:env:info | grep "GitLab info" -A 1
			docker exec $glmain bash -c "curl -s https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | bash"
			docker exec $glmain apt-get install gitlab-ce
		fi;
	;;
	b|B )
		# In case I need cron tutorial for backing up configurations:
		# http://www.pantz.org/software/cron/croninfo.html
		# crontab -e
		# 0 0 * * FRI docker exec -t gitlab-master gitlab-rake 
		backup
	;;
esac
