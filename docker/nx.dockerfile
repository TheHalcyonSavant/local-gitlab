# based on debian:9-slim
FROM nginx

# installing sshd
COPY .ssh /root/.ssh
RUN /root/.ssh/install
EXPOSE 22

ADD nx.sh /opt/bin/
ENTRYPOINT ["/opt/bin/nx.sh"]
