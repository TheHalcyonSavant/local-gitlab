# based on debian:9
FROM node

# installing sshd
COPY .ssh /root/.ssh
RUN /root/.ssh/install
EXPOSE 22

# installing mongodb-org
RUN apt-key adv --no-tty --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4 \
  && echo "deb http://repo.mongodb.org/apt/debian stretch/mongodb-org/4.0 main" | tee /etc/apt/sources.list.d/mongodb-org-4.0.list \
  && mkdir -p /data/db
RUN apt-get update && apt-get install -y apt-transport-https mongodb-org

ADD nm.sh /opt/bin/
# CMD ["/opt/bin/run_all"]
