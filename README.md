## Usage

On Windows start:

1. Start docker
2. Open VSCode in d:/gitlab/src/gitlab
3. Once docker is running, check containers status with `docker ps -a`
4. Then in terminal write:
    1. ./run.sh down
    2. ./run.sh up
5. Open https://spalna, and sign-in with: root/kocev321

The entire startup process could take 30 min.
